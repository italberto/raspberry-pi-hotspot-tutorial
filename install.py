#!/bin/env python3

# Author: Italberto F. Dantas
# Date: 10/11/2019

import subprocess as sub
import shlex
import os
import re
import time

class Conteudo(object):
    def __init__(self):
        pass
    
    def conteudo_ip_estatico(self,wifi_iface: str, cable_iface: str, ip: str) -> str:
        return 'interface {0}\nstatic ip_address={1}\ndenyinterfaces {0}\ndenyinterfaces {2}'.format(wifi_iface,ip,cable_iface)

    
    def conteudo_dhcpcd(self,wifi_iface: str, ip_inicial: str, ip_final: str, prefixo=16) -> str:
        subrede = '255.255.'
        if prefixo==16:
            subrede += '0.0'
        elif prefixo==24:
            subrede += '255.0'
        
        return 'interface={}\n  dhcp-range={},{},{},24h'.format(wifi_iface,ip_inicial,ip_final,subrede)


    def conteudo_interfaces(self,nome_ponte: str, wlan_iface:str, cable_iface: str) -> str:
        return '\n\nauto {0}\niface {0} inet manual\nbridge_ports {1} {2}'.format(nome_ponte,cable_iface,wlan_iface)


    def conteudo_hostapd(self,ssid: str, password: str) -> str:
        return '''
interface=wlan0
bridge=br0
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
ssid={}
wpa_passphrase={}
'''.format(ssid,password)




#------------------------------------------------------------------------------------------------
#                                    Funções Auxiliares
#------------------------------------------------------------------------------------------------
EXECUTAR_REAL = True

'''
    Print
'''
def printx(msg,color=''):

    if not color:
        print(msg)
    elif color == 'blue':
        print("\033[1;34;40m {}".format(msg))
    elif color == 'green':
        print("\033[1;32;40m {}".format(msg))
    elif color == 'yellow':
        print("\033[1;33;40m {}".format(msg))
    elif color == 'red':
        print("\033[1;31;40m {}".format(msg))

def alerta(msg):
    printx('\t\t'+msg,color='yellow')
    
def erro(msg):
    printx('\t'+msg,color='red')
    
def sucesso(msg):
    printx('\t'+msg,color='green')

def titulo(msg):
    printx(msg,color='blue')

'''
    Aceita entrada do usuário.
'''
def ler_entrada(msg,padrao=''):
    if padrao:
        msg = msg + ' ({}): '.format(padrao)
    else:
        msg = msg + ': '
    entrada = input(msg)

    if not entrada:
        entrada = padrao

    return entrada
    

'''
    Executa um comando passado como parâmetros no S.O.
'''
def executar(comando: str) -> str:
    if EXECUTAR_REAL:
        # Transforma a entada, o comando string, em uma lista.
        args = shlex.split(comando)
        # Executa um comando passado como parâmetros.
        result = sub.run(args,capture_output=True)
        
        return [result.returncode,str(result.stdout,encoding='utf8')]
    else:
        time.sleep(2)
        return [True,'Comando executado com sucesso.']
    
'''
    Cria um arquivo, se ele nÃ£o existir.
'''
def criar_arquivo(path: str) -> bool:
    if os.path.exists(path):
        return True
    else:
        try:
            os.mknod(path)
        except Exception as ele:
            print(e)
            return False
        return True
'''
    Adiciona conteúdo a um determinado arquivo texto.
'''
def adicionar_ao_arquivo(conteudo: str, path: str) -> bool:
    try:
        with open(path,'a+') as file:
            file.write(conteudo)
    except:
        return False
    return True

'''
    Verifica a existencia de um padrão em um arquivo.
'''
def existe_padrao(padrao: str,path: str) -> bool:
    resultado = None
    with open(path,'r') as file:
        resultado = file.read()
    return padrao in resultado

'''
    Substitui uma linha em um arquivo, passado um padrão.
'''
def substitui_linha(padrao: str, novo_texto: str, path: str) -> bool:
    try:
        resultado = ''
        with open(path,'r') as file:
            resultado = file.read()
            
        resultado = resultado.replace(padrao,novo_texto)
            
        with open(path,'w') as file:
            file.write(resultado)
            
        
    except Exception as e :
        alerta(e)
        return False
    return True

#--------------------------------------------------------------------------------
#                          Passos do script
#--------------------------------------------------------------------------------
DHCPCD_FILE = '/etc/dhcpcd.conf'
INTERFACES_FILE = '/etc/network/interfaces'
DNSMAQ_FILE = '/etc/dnsmasq.conf'
HOSTAPD_CONF_FILE = '/etc/hostapd/hostapd.conf'
HOSTAPD_MAIN_FILE = '/etc/default/hostapd'
SYSCTL_FILE = '/etc/sysctl.conf'
IPTABLES_RULE_SAVE_FILE = '/etc/iptables.ipv4.nat'
RCLOCAL_FILE = '/etc/rc.local'

# DHCPCD_FILE = './files/dhcpcd.conf'
# INTERFACES_FILE = './files/interfaces'
# DNSMASQ_FILE = './files/dnsmasq.conf'
# HOSTAPD_CONF_FILE = './files/hostapd.conf'
# HOSTAPD_MAIN_FILE = './files/hostapd'
# SYSCTL_FILE = './files/sysctl.conf'
# IPTABLES_RULE_SAVE_FILE = './files/iptables.ipv4.nat'
# RCLOCAL_FILE = './files/rc.local'


'''
    Atualiza as fontes dos repositório apt.
'''
def atualizar_fontes() -> bool:
    result = executar('apt-get update')
    if result[0]:
        return True
    return False
        
    
'''
    Instala os programas hostapd e dnsmasq.
    O parâmetros -y dispensa a interação com o usuário, 
    confirmando todas as solicitações.
'''
def instala_programas():
    result = executar('apt-get install -y dnsmasq hostapd bridge-utils')
    if result[0]:
        return True
    return False
    
'''
    Para os serviços
'''
def parar_servicos():
    result = executar('systemctl stop hostapd && systemctl stop dnsmasq')
    if result[0]:
        return True
    return False
    
'''
    Configura o endereÃ§o estático para a interface wlan.
'''
def configura_ip_estatico(wifi_iface: str, cable_iface: str, ip: str) -> bool:
    resultado = Conteudo().conteudo_ip_estatico(wifi_iface,cable_iface,ip)
    nome_arquivo = DHCPCD_FILE
    if not existe_padrao(resultado,nome_arquivo):
        if criar_arquivo(nome_arquivo):
            if adicionar_ao_arquivo(resultado,nome_arquivo):
                return True
            else:
                return False
        else:
            return False
    else:
        alerta('O arquivo {} não foi modificado pois já atende aos requisitos.'.format(DHCPCD_FILE))
        return True
        
'''
    Configura dhcp. O prefixo deve ser 16 ou 24
'''
def configurar_dhcp(wifi_iface: str, ip_inicial: str, ip_final: str,prefixo: int) -> bool:
    resultado = Conteudo().conteudo_dhcpcd(wifi_iface,ip_inicial,ip_final,prefixo)
    nome_arquivo = DNSMASQ_FILE
    if not existe_padrao(resultado,nome_arquivo):    
        if adicionar_ao_arquivo(resultado,nome_arquivo):
            return True
        return False
    else:
        alerta('O arquivo {} não foi alterado pois já atende aos requisitos.'.format(DNSMASQ_FILE))
        return True
    
'''
    Configurar hostapd.conf
'''
def configurar_hostapd(ssid: str, password: str) -> bool:
    resultado = Conteudo().conteudo_hostapd(ssid,password)
    nome_arquivo = HOSTAPD_CONF_FILE
    if criar_arquivo(nome_arquivo):
        if not existe_padrao(resultado,nome_arquivo):        
            if adicionar_ao_arquivo(resultado,nome_arquivo):
                return True
            return False
        alerta('O arquivo {} não foi alterado pois já atende aos requisitos.'.format(HOSTAPD_CONF_FILE))
        return True
    else:
        return False
    
'''
    Informar arquivo de Configuração
'''
def informar_arquivo_hostapd() -> bool:
    if substitui_linha('#DAEMON_CONF=""','DAEMON_CONF="{}"'.format(HOSTAPD_CONF_FILE),HOSTAPD_MAIN_FILE):
        return True
    return False


'''
    Configurar encaminhamento de tráfego
'''
def configurar_encaminhamento_trafego() -> bool:
    if substitui_linha('#net.ipv4.ip_forward=1','net.ipv4.ip_forward=1',SYSCTL_FILE):
        return True
    return False

'''
    Adicionar regra ao iptables
'''
def adicionar_regra_iptables(cable_iface: str) -> bool:
    result = executar('iptables -t nat -A POSTROUTING -o {} -j MASQUERADE')
    if result[0]:
        return True
    return False

'''
    Salvar regra iptables
'''
def salvar_regra_iptables() -> bool:
    result = executar('sh -c "iptables-save > {}"'.format(IPTABLES_RULE_SAVE_FILE))
    if result[0]:
        return True
    return False

'''
    Configurar o iptables para a versão legacy
'''
def configurar_iptables_legacy() -> bool:
    result = executar('iptables --version')
    if 'legacy' not in result[1]:
        if executar('update-alternatives --set iptables /usr/sbin/iptables-legacy')[0]:
            return True
        return False
    

'''
    Configurar rc.loca para recarregar a regra de firewall
    sempre que a máquina for reiniciada.
'''
def configurar_rclocal() -> bool:
    resultado = '\n\niptables-restore < {}'.format(IPTABLES_RULE_SAVE_FILE)
    nome_arquivo = RCLOCAL_FILE
    if not existe_padrao(resultado,nome_arquivo):
        if adicionar_ao_arquivo(resultado,nome_arquivo):
            return True
        return False
    else:
        alerta('O arquivo {} não foi alterado pois já atende aos requisitos.'.format(RCLOCAL_FILE))
    return True

'''
    Configura as pontes, necessárias para habilitar a conexão com a internet
'''
def configura_pontes(nome_ponte: str,cable_iface: str) -> bool:
    result1 = executar('brctl addbr {}'.format(nome_ponte))
    result2 = executar('brctl addif {} {}'.format(nome_ponte,cable_iface))
    
    
    if result1 and result2:
        return True
    return False
    
'''
    Configurar interfaces
'''
def configurar_interfaces(nome_ponte: str, wlan_iface: str, cable_iface: str) -> bool:
    resultado = Conteudo().conteudo_interfaces(nome_ponte,wlan_iface,cable_iface)
    nome_arquivo = INTERFACES_FILE
    if not existe_padrao(resultado,nome_arquivo):
        if adicionar_ao_arquivo(resultado,nome_arquivo):
            return True
        return False
    else:
        alerta('O arquivo {} não foi alterado pois já atende aos requisitos.'.format(INTERFACES_FILE))
    return True

'''
    Reestabelecer serviços
'''
def restabelecer_servicos() -> bool:
    result1 = executar('systemctl start dnsmasq')
    result2 = False
    
    if not executar('systemctl start hostapd')[0]:
        executar('systemctl unmask hostapd')
        executar('systemctl enable hostapd')
        result2 = executar('systemctl start hostapd')[0]
        return result1[0] and result2[0]
    return True
'''
    Lê os parâmetros necessários para configurar o roteador.
'''
def ler_parametros():    
    wlan_iface = ler_entrada('Informe a interface wifi que será utilizada',padrao='wlan0')
    cable_iface = ler_entrada('Informe a interface que irá estabelecer a conexão com a internet',padrao='eth0')
    bridge_iface = ler_entrada("Informe o nome que será utilizado na ponte",padrao='br0')
    ip_router = ler_entrada("Informe o endereço ip do reoteador",padrao='192.168.0.10')
    ip_range =[ler_entrada('Faixa inicial de ips',padrao='192.168.0.11'),
               ler_entrada('Faixa final de ips',padrao='192.168.0.30')]
    ssid = ler_entrada('SSID da rede',padrao='PiRouter')
    senha = ler_entrada('Senha da rede',padrao='raspberrypi')

    return {'wlan_iface':wlan_iface,
            'cable_iface':cable_iface,
            'bridge_iface':bridge_iface,
            'ip_router':ip_router,
            'ip_range':ip_range,
            'ssid':ssid,
            'senha':senha}
'''
    Função principal.
'''
def main():
    
    args = ler_parametros()
    
    
    titulo('1. Atualização das fontes.')
    if atualizar_fontes():
        sucesso('Os programas foram instalados.')    
    else:
        erro('Houve um erro ao tentar atualizar as fontes.')
    
        
    titulo('2. Instalar o hostapd e ndsmasq e bridge-utils')
    if instala_programas():
        sucesso('Os programas foram instalados.')
    else:
        erro('Houve um erro ao tentar instalar os programas.')
    
    if parar_servicos():
        sucesso('Os programas foram instalados.')
    else:
        erro('Houve um erro ao parar os serviços!')
        
    titulo('3. Configurar um ednereço IP estático para a interface sem fio principal')
    if configura_ip_estatico(args['wlan_iface'], args['cable_iface'], args['ip_router']):
        sucesso('IP estático configurado.')
    else:
        erro('Houve um erro ao configurar o ip estático')

    titulo('4. Configurar o DHCP (dnsmasq)')
    if configurar_dhcp(args['wlan_iface'],args['ip_range'][0],args['ip_range'][1],24):
        sucesso('DHCP configurado.')
    else:
        erro('Houve um erro ao configurar o dhcp.')
        
        
    titulo('5. Configuar o access point.')    
    if configurar_hostapd(args['ssid'],args['senha']):
        sucesso('Hostapd configurado.')
    else:
        erro('Houve um erro ao configurar o hostapd.')
        
        
    if informar_arquivo_hostapd():
        sucesso('Arquivo de configuração do hostapd pronto.')
    else:
        erro('Houve um erro ao configurar o arquivo hostapd.')
        
    titulo('6. Setup do encaminhamento de tráfego.')    
    if configurar_encaminhamento_trafego():
        sucesso('Configuração de encaminhamento de tráfego realizada.')
    else:
        erro('Houve um erro ao realizar a Configuração do encaminhamento de tráfego.')
        
        
    titulo('7. Adicionando uma nova regra no iptables.')    
    if adicionar_regra_iptables(args['cable_iface']):
        sucesso('Regrad iptables adicionadas.')
    else:
        erro('Houve um erro ao adicionar as regras do iptables.')
        
        
    if salvar_regra_iptables():
        sucesso('Regras do iptables salvas.')
    else:
        erro('Houve um erro ao salvar as regras do iptables')
        
        
    if configurar_iptables_legacy():
        sucesso('Iptables legacy configurado.')
    else:
        erro('Houve um erro ao configurar o iptables legacy.')
        
        
    if configurar_rclocal():
        sucesso('Entrada de Configuração do iptables adicionada ao rc.local')
    else:
        erro('Houve um erro ao adicionar o arquivo de regras do iptables ao rc.local')
        
    titulo('8. Habilitar a conexão com a internet.')    
    if configura_pontes(args['bridge_iface'],args['cable_iface']):
        sucesso('Ponte configurada.')
    else:
        erro('Houve um erro ao configurar a ponte.')
        
        
    if configurar_interfaces(args['bridge_iface'], args['wlan_iface'], args['cable_iface']):
        sucesso('Interfaces de ponte configuradas.')
    else:
        erro('Houve um erro ao configurar as interfaces de ponte.')
        
    titulo('9. Restaurando os serviços')    
    if restabelecer_servicos():
        sucesso('serviços reestabelecidos.')
    else:
        erro('Houve um erro ao reestabelecer os serviços.')

if __name__ == '__main__'
    main()