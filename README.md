# Configurando um Raspberry Pi commo WiFi Hotspot

## 1. Atualização das fontes

Primeiro de tudo, é necessário atualizar a lista de pacotes.

    $ sudo apt-get update
    $ sudo apt-get upgrade

## 2. Instalar o hostapd e dnsmasq
Em seguida, precisamos instalar os dois programas que irão transformar o raspberry em roteador. O [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) e o [hostapd](https://w1.fi/hostapd/).

    $ sudo apt-get install dnsmasq hostapd

Depois de instalados, precisamos desativá-los, para que possamos alterar as configurações sem nenhum problema. Ao final iremos 

    $ sudo systemctl stop hostapd
    $ sudo systemctl stop dnsmasq    

## 3. Configurar um endereço IP estático para a interface wlan0 
Vamos configuar um endereço de ip estático. Assumindo que nossa rede terá o seguinte padrão de sub-rede: **192.168.0.0/24**. Ou seja, todos os endereços atribuídos aos hosts que se conectarem, terão o formato 192.168.0.n.

    $ sudo nano /etc/dhcpcd.conf

~~~~ config
interface wlan0
static ip_address=192.168.0.10/24
denyinterfaces eth0
denyinterfaces wlan0
~~~~

##  4. Configure the DHCP server (dnsmasq)

A configuração do DHCP é simples e resume-se a adicionar duas linhas em um único arquivo. 

    $ sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
    $ sudo nano /etc/dnsmasq.conf

~~~~ config
interface=wlan0
  dhcp-range=192.168.0.11,192.168.0.30,255.255.255.0,24h
~~~~

## 5. Configurar o access point

O arquivo hostapt.conf ainda não existe. Para criá-lo basta abri-lo com o nano ou com qualquer outro editor de sua preferência. 

    $ sudo nano /etc/hostapd/hostapd.conf

Adicione as seguintes linhas a ele:

~~~~
    interface=wlan0
    bridge=br0
    hw_mode=g
    channel=7
    wmm_enabled=0
    macaddr_acl=0
    auth_algs=1
    ignore_broadcast_ssid=0
    wpa=2
    wpa_key_mgmt=WPA-PSK
    wpa_pairwise=TKIP
    rsn_pairwise=CCMP
    ssid=NETWORK
    wpa_passphrase=PASSWORD
~~~~

Substituindo o nome da rede em ssid e a senha em wpa_passphrase.
Agora precisamos dizer ao hostapd que esse arquivo que acabamos de criar é o arquivo de comfiguração do serviço. Para isso, encontre no arquivo a seguir essa linha: **#DAEMON_CONF=""**, descomente-a e adicione o caminho do arquivo que acabamos de criar.

    $ sudo nano /etc/default/hostapd

A linha deverá ficar assim:

     DAEMON_CONF="/etc/hostapd/hostapd.conf"

## 6. Setup do encaminhamento de tráfego

No arquivo a seguir descomente a seguinte linha **#net.ipv4.ip_forward=1**:

    $ sudo nano /etc/sysctl.conf

Ela deverá ficar assim:

    net.ipv4.ip_forward=1

## 7. Adicionar uma nova regra ao iptables (firewall)

Vamos adicionar uma nova regra ao iptables que irá fazer o redirecionamento do tráfego de saída:

    $ sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

Salvamos essa regra com o seguinte comando:

    $ sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"

E para que nossa regra seja carregada sempre que o sistema for reiniciado, devemos adicionar a seguinte linha ao arquivo **/etc/rc.local** logo após a linha **exit 0**.

    iptables-restore < /etc/iptables.ipv4.nat

As versões mais recentes do iptables não aceitam o parâmetro MASQUERADE. Para que esse comando funcione, precisamos fazer um downgrade em sua versão. Isso é bem simples, basta executar o seguinte comando e escolher uma versão que seja compatível.

    $ update-alternatives –config iptables

## 8. Habilitar a conexão com a internet

A última parte da configuração é a criação de uma ponte entre a conexão de entrada e a de saída, ou seja a eht0 e a wifi0.

    $ sudo apt-get install bridge-utils

    $ sudo brctl addbr br0

    $ sudo brctl addif br0 eth0

Por fim, adicione as linhas a seguir ao arquivo de interfaces.

    $ sudo nano /etc/network/interfaces

~~~~
auto br0
iface br0 inet manual
bridge_ports eth0 wlan0
~~~~

## 9. Restaurando os serviços

Para habilitar os serviços novamente (foram desabilitados no início da configuração.)

Habilitar o serviço de ap.

    $ sudo systemctl start hostapd

Caso o comando acima apresente algum erro basta executar essas duas linha a seguir e repetir o passo anterior.


    $ sudo systemctl unmask hostapd
    $ sudo systemctl enable hostapd


Habilitar o serviço de dhcp.
    
    $ sudo systemctl stop dnsmasq

## 10. Reboot

    $ shutdown -r now

## 11. Restaurando a conexão com a internet

Se você não quer usar seu raspberry exclusivamente como roteador, você pode desabilitar essa funcionalidade a qualquer momento para poder conectar-se novamente a uma rede wifi novamente. Para isso, basta comentar as linhas adicionadas no arquivo **/etc/dhcpcd.conf** e parar os serviços:

    $ sudo systemctl start dnsmasq

    $ sudo systemctl start hostapd

Para voltar a funcionar, bastar fazer o inverso.

### Referências
* [How to use your raspberry pi as a wireless access point](https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/)
* [Failed to start hostapd.service: Unit hostapd.service is masked.](https://github.com/raspberrypi/documentation/issues/1018)